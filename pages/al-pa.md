---
title: AL Fattura PA
layout: application
---
<html>
    <script>
      <!-- DA INSERIRE I RELATIVI LINK -->
        window.onload = function (){
            var instructions = document.getElementById("instructions")
            var termsconditions = document.getElementById("terms&conditions")
            instructions.href="al-pa.html"
            termsconditions.href="al-pa.terms&conditions.html"
        }
    </script>
    <body>
      <div class="card position-fixed d-none d-xl-block" style="width: 18rem; right: 5%;">
        <div class="card-body">
          <!-- TOC -->
          <a href="#AL Fattura PA" class="font-weight-bold">AL PA</a><br>
          <a href="#11-esigenze">- Esigenze</a><br>
          <a href="#12-funzionalità-principali">- Funzionalità principali</a><br>
          <a href="#13-vantaggi">- Vantaggi</a><br>
          <a href="#14-setup">- Setup</a><br>        
          <!-- /TOC -->
        </div>
      </div>
    </body>
</html>


# AL Fattura PA

## 1.1. Esigenze

L'esigenza che copre il modulo di fatturazione elettronica è quello di generare automaticamente i file *.xml delle fatture elettroniche da inviare tramite lo SDI o un intermediario.

## 1.2. Funzionalità principali

Il modulo estende le funzionalità standard di business central che riguardano la gestione delle fatturazione elettronica e garantisce l'aggiornamento delle funzionalità fornite alle versioni successive di Business Central (anche in cloud) senza che sia necessario un specifico progetto di migrazione.

Le principali funzionalità offerte dall'app sono le seguenti:

   1. a livello di **fattura di vendita registrata** c'è un pulsante **Invia fattura XML** che, una volta premuto, genera e memorizza in un percorso specificato a setup, file collegato alla fattura, stacca l'identificativo univoco della fattura per lo SDI (5 caratteri alfanumerici), e lo memorizza nella tabella delle fatture registrate. In qualsiasi momento è possibile rigenerare il file cliccando nuovamente sul pulsante, mantenendo lo stesso codice SDI. Nel caso in cui manchino alcune informazioni per generare il file, è possibile quindi modificare la fattura per inserire le informazioni e rigenerare il file.
   2.  a livello di **Nota di Credito di Vendita registrata** c'è un pulsante **Invia fattura XML** che, una volta premuto, genera e memorizza in un percorso spcificato a setup, file collegato alla fattura, stacca l'identificativo univoco della fattura per lo SDI (5 caratteri alfanumerici), e lo memorizza nella tabella delle fatture registrate. In qualsiasi momento è possibile rigenerare il file cliccando nuovamente sul pulsante, mantenendo lo stesso codice SDI. Nel caso in cui manchino alcune informazioni per generare il file, è possibile quindi modificare la fattura per inserire le informazioni e rigenerare il file.
   3.  Generazione dei file per l'invio allo sdi degli acquisti intracomunitari.
   4. Una volta che sarà completata la migrazione a Business Central esiste la possibilità di integrare l'applicazione con l'app per la dichiarazione d'intento (AL VAEE).

## 1.3. Vantaggi

L'applicazione è sviluppata in AL, la nuova tecnologia Microsoft che consente un agile aggiornamento della piattaforma Business central. 

Questo consentirà di tenere aggiornata la piattaforma all'ultima versione disponibile e quindi di poter sfruttare tutti i miglioramenti e aggiornarnamenti che la Microsoft apporta alla piattaforma Business central.

## 1.4. Setup

Cercare e visualizzare pagina profili invio.

<img class= "zoom" src = "../img/al-pa/ProfiInvDoc.png">
<!-- ![](../img/al-pa/2022-06-01-14-28-57.png) -->

Impostare su disco l'opzione **documento elettronico** e su formato **FATTURAPA**.

<img class= "zoom" src = "../img/al-pa/ProfiInvDocScheda.png">
<!-- ![](../img/al-pa/2022-06-01-14-30-20.png) -->

Cercare a questo punto **formati documento elettronico** e impostare le corrispondenti codeunit dell'app **AL-PA** in corrispondenza della Fattura di vendita, Nota di Credito e Convalida Sales Validation.

<img class= "zoom" src = "../img/al-pa/FormDocElet.png">
<!-- ![](../img/al-pa/2022-06-01-14-37-52.png) -->

Cercare pagina '*Setup Fatt. Elettronica*' e impostare opportunamente i parametri ivi contenuti:
  
   - imposta e soglia bollo
   - percorso base di salvataggio degli *.xml.
   - nome template registrazione incassi (in caso di nuova installazione, aprire il batch di registrazione incassi per generarne uno di default)
   - nome batch registrazione incassi per giroconto autofattura
   - Crea Fatt. PA nelle Fattura di vendita registrate: quando selezionato viene generato e automaticamente salvato nel percorso predefinito l'xml delle fattura di vendita.
   - Crea Cr. PA nelle nota di credito di vendita registrate: quando selezionato viene generato e automaticamente salvato nel percorso predefinito l'xml delle nota di credito.
   - Conto C/G autofattura: se compilato questo conto viene utilizzato per generare le righe dell'autofattura corrispondenti alle righe di acquisto. Nel caso in cui invece non sia compilato il sistema recupererà il conto c/g dal conto acquisti del setup registrazioni corrispondente (riga per riga) alla fattura di acquisto.
   - testo esteso automaticamente compilato in fase di generazione autofattura. 
   - testo esteso per Split Payment.

Di seguito gli screenshot relativi al testo esteso con in campi opportunamente compilati.

<img class= "zoom" src = "../img/al-pa/CodTestiStand.png">
<!-- ![](../img/al-pa/2022-07-05-15-21-23.png) -->


I testi estesi dovranno essere compilati in modo simile a questi. Di seguito i due screenshot di esempio.

<img class= "zoom" src = "../img/al-pa/Autofattura.png">
<img class= "zoom" src = "../img/al-pa/FVPA.png">
<!-- ![](../img/al-pa/2022-06-01-14-59-19.png)
![](../img/al-pa/2022-07-05-15-23-28.png) -->

Il setup PA completato sarà ad esempio come di seguito illustrato:

<img class= "zoom" src = "../img/al-pa/SetupPA.png">
<!-- ![](../img/al-pa/2022-07-05-15-24-41.png) -->

Su Setup contabilità Clienti impostare il Progressivo PA.

<img class= "zoom" src = "../img/al-pa/SetConCli.png">
<!-- ![](../img/al-pa/2022-06-01-15-30-56.png) -->

Il quale dovrà contenere come dato un <a href="#numero-intero">numero intero</a>:

<img class= "zoom" src = "../img/al-pa/NrSerie.png">
<!-- ![](../img/al-pa/2022-06-01-15-32-44.png). -->

A questo punto si può procedere a testare la generazione dell'xml PA usando la funzione standard **Invia a...** che si trova nelle fatture vendite registrate. Nel caso ci siano errori il sistema avvisa con l'elenco delle azioni necessarie da completare per poter procedere alla generazione e i link da seguire per effettuare le correzioni opportune.

<img class= "zoom" src = "../img/al-pa/ErrorMess.png">
<!-- ![](../img/al-pa/2022-06-01-15-35-21.png) -->

Per poter procedere è necessario completare il setup delle condizioni di pagamento, metodo di pagamento e della Categorie reg. business IVA.

<img class= "zoom" src = "../img/al-pa/CondPag.png">
<!-- ![](../img/al-pa/2022-06-01-15-42-09.png) -->

<img class= "zoom" src = "../img/al-pa/MetPag.png">
<!-- ![](../img/al-pa/2022-06-01-15-43-34.png) -->

Nella pagina '*Categorie reg. Business IVA*' è presente il flag **Genera autofattura PA**. Se lo impostate, quando registrate una fattura di acquisto per fornitori con quel codice di cat. reg. business iva, il sistema genera una autofattura di vendita che, nel caso del reverse charge, non crea nessun movimento (esistono già i movimenti di reverse charge), mentre nel caso normale genera un'autofattura provvisoria che potete modificare prima di procedere alla registrazione.

<img class= "zoom" src = "../img/al-pa/CatRegBussIVA.png">

## Numero intero

Per convertire un codice alfanumerico in intero, nella pagina *'Setup Fatt. elettronica'*, è presente il campo **Ultimo nr. progressivo**: Non appena si valida il codice nel campo verrà visualizzato un messaggio con il numero intero corrispondente.

<img class= "zoom" src = "../img/al-pa/NrProgressivo.png">

<!-- [Guida pratica all'uso](../pdf/AL-PA.pdf)   -->


<!-- Script per usare medium zoom per immagini -->
<script src="../JS/medium-zoom-min.js"></script>
<script src="../JS/medium-zoom-luca.js"></script>