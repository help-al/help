---
layout: application
---
<html>
    <script>
      <!-- DA INSERIRE I RELATIVI LINK -->
        window.onload = function (){
            var instructions = document.getElementById("instructions")
            var termsconditions = document.getElementById("terms&conditions")
            instructions.href="al-combship.html"
            termsconditions.href="al-combship.terms&conditions.html"
        }
    </script>
<body>
    <div class="card position-fixed d-none d-xl-block" style="width: 18rem; right: 5%;f">
  <div class="card-body">
    
<!-- TOC -->
<a href="#terms-and-conditions" class="font-weight-bold">Terms and Conditions</a><br>
<a href="#1-definitions">1. Definitions</a><br>
<a href="#2-scope-of-the-conditions">2. Scope of the Conditions</a><br>
<a href="#3-content-sent-by-the-users">3. Content sent by the Users</a><br>
<a href="#4-industrial-and-intellectual-property-rights">4. Industrial and intellectual property rights</a><br>
<a href="#5-exclusion-of-warranty">5. Exclusion of warranty</a><br>
<a href="#6-limitation-of-liability">6. Limitation of liability</a><br>
<a href="#7-force-majeure">7. Force majeure</a><br>
<a href="#8-links-to-third-party-web-sites">8. Links to third-party web sites</a><br>
<a href="#9-privacy">9. Privacy</a><br>
<a href="#10-applicable-law-and-jurisdiction">10. Applicable law and jurisdiction</a><br>

<!-- /TOC -->
  </div>
</div>
</body>
</html>


Terms and Conditions
=

This document contains the general terms and conditions under which users are offered the use of [applications](https://dynamicsapplab.com/apps) that provide software and application services for Business Central 365.

# 1. Definitions

To allow a complete understanding and acceptance of these terms and conditions, the following terms, in the singular and in the plural, shall have the meaning indicated below:
- Owner: Dynamics App Lab limited, with registered address at 13 Adelaide Road, Dublin, D02 P950, Ireland
- Application: one of the site's [applications](https://dynamicsapplab.com/apps).
- User: any person who accesses and uses the Application
- Conditions: this contract which governs the relationship between the Owner and the Users.
  
# 2. Scope of the Conditions

The use of the Application implies full acceptance of the Conditions by the User. Should the User not accept the Conditions and / or any other note, legal notice, information published or referred to therein, the User shall not use the Application or the services related.

The Owner may amend the Conditions at any time. The changes shall be effective from the time they are published in the Application.

Before using the Application, the User is required to read the Conditions carefully save or print them for future reference.

The Owner reserves the right to change, at his own discretion and at any time, the graphic interface of the Application, the Contents and their organisation, as well as any other feature that characterises the functionality and management of the Application, communicating to the User the relative instructions, when necessary.

# 3. Content sent by the Users

The User can upload Content on the Application, provided that it is not illegal (e.g. obscene, intimidating, defamatory, pornographic, abusive or for any reason illegal or in violation of privacy, the intellectual and / or industrial property rights of the Owner and / or third parties), misleading, or is not otherwise harmful to the Owner and / or third parties or contains viruses, political propaganda, commercial solicitation, mass e-mail or any other form of spamming. In the event of a dispute by a third party regarding any announcement or conduct related to it, the User assumes full liability and undertakes to hold the Holder harmless from any damage, loss or expense.

The User guarantees that the Contents are sent to the Application through his account from a natural person of legal age. For natural persons under legal age, the sending of Contents must be examined and authorised by the parents or by those exercising parental authority.

The User is the sole and exclusive responsible for the use of the Application with regard to the publication, consultation, management of the Content and contact between Users and is therefore the sole guarantor and responsible for the correctness, completeness and lawfulness of the Contents and its own behaviour.

It is forbidden to use an e-mail address that is not owned by the User, to use the personal data and credentials of another User in order to use his identity, or in any other way to declare false information about the origin of the Contents.

The Owner is unable to ensure timely control over the Content received and reserves the right at any time to cancel, move or modify the Content, which, at its discretion, appears to be illegal, abusive, defamatory, obscene or prejudicial to the right to author and trademarks or in any case unacceptable.

Users grant the Owner a non-exclusive right of use on the Content sent, without limitations of geographical areas. The Owner may therefore, directly or through trusted third ies, use, modify, copy, transmit, extract, publish, distribute, publicly perform, disseminate, create derivative works, host, index, store, note, encode, modify and adapt (including without limitation the right to adapt for transmission in any form of communication) in any form, any Content (including images, messages, including audio and video) that should be sent by the User, including through third parties.

The Content sent will not be returned and the Owner will not be liable towards Users for the loss, modification or destruction of the transmitted Content.

It is expressly forbidden, unless explicitly authorised by the Owner: i) the use of automatic announcement uploading systems, except those expressly authorised, ii) serial publication and / or management of advertisements for third parties by any means or method, iii) resell the Owner's services to third parties.

# 4. Industrial and intellectual property rights

The Owner declares to be the owner and / or licensee of all intellectual property rights relating to and / or relating to the Application and / or the Content available on the Application. Therefore, all the trademarks, figurative or word and all other signs, commercial names, service marks, word marks, commercial names, illustrations, images, logos, contents relating to the Application are and remain the property of the Owner or its licensees and are protected by the laws in force on the trademarks and by the relative international treaties.

The Conditions do not grant the User any license for use relating to the Application and / or to individual contents and / or materials available therein, unless otherwise regulated.

Any reproductions in any form of the explanatory texts and the Contents of the Application, if not authorised, will be considered violations of the intellectual and industrial property right of the Owner.

# 5. Exclusion of warranty

The Application is provided "as is" and "as available" and the Owner does not provide any explicit or implicit guarantee in relation to the Application, nor does it provide any guarantee that the Application will satisfy the needs of the Users or that it will not have never interrupt or be error-free or free of viruses or bugs.

The Owner will endeavour to ensure that the Application is available continuously 24 hours a day, but cannot in any way be held responsible if, for any reason, the Application is not accessible and / or operational at any time or for any period . Access to the Application may
be suspended temporarily and without notice in the event of system failure, maintenance,
repairs or for reasons wholly unrelated to the owner's will or due to force majeure events.

# 6. Limitation of liability

The Owner shall not be held liable towards the User, except in the case of wilful misconduct or
gross negligence, for disservices or malfunctions connected to the use of the internet
outside of its own control or that of its suppliers.

Furthermore, the Owner will not be liable for damages, losses and costs incurred by the User
as a result of failure to execute the contract for reasons not attributable to him.

The Owner assumes no responsibility for any fraudulent or illegal use that may be made by
third parties of credit cards and other means of payment,

The Owner shall not be held liable for:
- any loss of business opportunities and any other loss, even indirect, possibly suffered by the
User that is not a direct result of the breach of contract by the Owner
- incorrect or unsuitable use of the Application by Users or third parties
In no case the Owner shall be held liable for a sum greater than twice the cost paid by the User.

# 7. Force majeure

The Owner shall not be held responsible for the failure or late fulfilment of its obligations, due to
circumstances beyond its reasonable control due to events of force majeure or, in any
case, to unforeseen and unforeseeable events and, in any case, independent of its will.

The fulfilment of the obligations by the Owner shall be considered suspended for the period in
which events of force majeure occur.

The Owner will perform any act in his power in order to identify solutions that allow the correct
fulfilment of his obligations despite the persistence of events due to force majeure.

# 8. Links to third-party web sites

The Application may contain links to third-party web sites / applications. The Owner has no
control over them and, therefore, is in no way responsible for the contents of these sites /
applications.

Some of these links may link to third-party sites / applications that provide services through the
Application. In these cases, the general conditions for the use of the site / application and
for the use of the service prepared by the third parties will be applied to the individual
services, with respect to which the Owner assumes no responsibility.

# 9. Privacy

The protection and processing of personal data will be in accordance with the Privacy Policy,
which can be consulted on this [page](https://dynamicsapplab.com/privacy)

# 10. Applicable law and jurisdiction

The Conditions are subject to Italian law.
For any dispute concerning the application, execution and interpretation of these Conditions,
the jurisdiction shall be upon the courts where the Owner is based.

_Latest update: 13/11/2023_
