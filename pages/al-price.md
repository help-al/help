---
title: AL Price
layout: application
---
<html>
    <script>
      <!-- DA INSERIRE I RELATIVI LINK -->
        window.onload = function (){
            var instructions = document.getElementById("instructions")
            var termsconditions = document.getElementById("terms&conditions")
            instructions.href="al-price.html"
            termsconditions.href="al-price.terms&conditions.html"
        }
    </script>
<body>
    <div class="card position-fixed d-none d-xl-block" style="width: 18rem; right: 5%;">
  <div class="card-body">

<!-- TOC -->
<a href="#al-price" class="font-weight-bold">AL Price</a><br>
<a href="#introduzione">- Introduzione</a><br>
<a href="#guida-allutilizzo">- Guida all'utilizzo</a><br>
<!-- /TOC -->
  </div>
</div>
</body>
</html>


# AL Price

## Introduzione

Dopo aver installato l'app i seguenti campi saranno visibili nei documenti di vendita e acquisto:

In testata:
- Sconto composto predefinito
- Sconto fattura % pagamenti
- Sconto fattura % manuale

Sulle righe:
- Sconto composto
- Sconto composto forzato
- Prezzo unitario forzato

In questo modo sarà più facile monitorare i prezzi e gli sconti di ogni fattura.


## Guida all'utilizzo

Per inserire uno sconto di default in tutte le righe di vendita (o acquisto) sarà sufficiente compilare il campo **Sconto composto predefinito**. In questo modo il campo **Sconto composto forzato** delle righe verrà compilato con il nuovo valore aggiunto. 
Fintanto che questo campo è definito, lo sconto della riga dipenderà da questo. Per tornare alla funzionalità standard è necessario sbiancare il campo.

Allo stesso modo, funziona anche il **Prezzo unitario forzato**: quando è inserito, il prezzo di quella riga di vendita (o acquisto) è il prezzo inserito in questo campo. Se viene sbiancato, la funzionalità torna standard.

Per quanto riguarda i campi **Sconto fattura % pagamenti** e **Sconto fattura % manuale** invece, la loro funzionalità consiste nel gestire lo sconto totale della fattura,
il quale verrà distribuito nelle righe in aggiunta allo sconto già presente. Il campo **% sconto fattura** (standard) sarà uguale alla somma di questi due campi.
Mentre per il campo **Sconto fattura % manuale** è sufficiente immettere il valore, per il campo **Sconto fattura % pagamenti** è necessario inserire la percentuale di sconto desiderata nel campo **Sconto fattura %** della pagina *Condizioni di pagamento* in corrispondenza della riga con lo stesso codice scelto nel documento.

<img class= "zoom" src = "../img/al-price/Paymentterms.png">

<!-- ![](../img/al-price/2022-05-04-12-35-37.png) -->

Il valore del campo **% sconto fattura** viene aggiornato ogni volta che si cambia il codice di condizioni pagamento, oppure che si modifica il campo Sconto fattura % manuale o al momento del rilascio. 

La funzionalità all'interno delle **fatture di vendita** è leggermente diversa in quanto il campo *"Sconto fattura % manuale"* viene calcolato in base agli ordini di vendita associati alle righe in fattura. Perciò, se la fattura è associata a più ordini di vendita il programma calcola lo sconto totale della fattura e imposta nel campo *"Sconto fattura % manuale"* la differenza tra lo sconto totale calcolato e lo sconto nel campo *"Sconto fattura % pagamenti"*.


<!-- Script per usare medium zoom per immagini -->
<script src="../JS/medium-zoom-min.js"></script>
<script src="../JS/medium-zoom-luca.js"></script>