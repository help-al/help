---
title: AL Combine Shipment
layout: application
---
<html>
    <script>
      <!-- DA INSERIRE I RELATIVI LINK -->
        window.onload = function (){
            var instructions = document.getElementById("instructions")
            var termsconditions = document.getElementById("terms&conditions")
            instructions.href="al-combship.html"
            termsconditions.href="al-combship.terms&conditions.html"
        }
    </script>
<body>
    <div class="card position-fixed d-none d-xl-block" style="width: 18rem; right: 5%;">
  <div class="card-body">

<!-- TOC -->
<a href="#al-combine-shipment" class="font-weight-bold">AL Combine Shipment</a><br>
<a href="#fatture-cumulative">- Fatture Cumulative</a><br>


<!-- /TOC -->
  </div>
</div>
</body>
</html>

# AL Combine Shipment

## Fatture Cumulative

Queste categorie permettono di raggruppare le spedizioni facendo riferimento a campi specifici impostati a setup.
Nella pagina *'Setup raggruppamenti spedizioni'* è possibile selezionare i campi obbligatori e facoltativi con i quali si desidera poter combinare le spedizioni.  
Inoltre, è anche possibile scegliere se si desidera validare il campo stesso nella testata della fattura o permettere la sola assegnazione.  
**NB**: La validazione viene usata come valore di default, tuttavia è necessario prestare attenzione all'ordine (definibile dalla sequenza) con cui vengono validati i campi, altrimenti è possibile che alcune informazioni vadano sovrascritte dalla validazione di altri campi.

<img class= "zoom" src = "../img/al-combship/SetRagSped.png">

Se si desidera utilizzare il campo **VAT Exemption No.** è necessario inserire anche i campi **VAT Bus. Posting Group** e **VAT Exempt.Int.Reg.No.**.  

Il campo **'Nome campo testata fattura'** è necessario per la mappatura del campo che si vuole utilizzare come criterio di spaccatura. Alcuni campi infatti possono avere nomi diversi, perciò è obbligatorio inserire il nome del campo corrispondente della testata della fattura. Questo, per la maggior parte dei campi, è automatico; tuttavia per alcuni campi la scelta è ambigua e per questo viene richiesto all'utente di specificare il campo desiderato.  
Per quanto riguarda i campi delle righe non presenti nella testata, è possibile utilizzarli come soli criteri di spaccatura, non verrà perciò fatta nè l'assegnazione nè la validazione. Per questi campi, il campo *'Nome campo testata fattura'* va lasciato vuoto.

Una volta impostati i campi, è possibile creare nuovi raggruppamenti nella pagina *'Lista raggruppamenti spedizioni'*.  
Per ogni raggruppamento, oltre ai campi obbligatori, si possono impostare altri campi e creare così raggruppamenti diversi a seconda delle esigenze.  
Una volta terminato di inserire i campi che si desiderano in un certo raggruppamento è importante dare al campo *'Stato'* il valore di **Certificato**.

<img class= "zoom" src = "../img/al-combship/ListaRaggSped.png">
<img class= "zoom" src = "../img/al-combship/RaggSped.png">

Il campo *'Nr. raggruppamento spedizione'* verrà compilato in automatico nell'**Ordine di vendita**, alla validazione del cliente, con il valore presente nell'anagrafica del cliente o, se non fosse presente nel cliente, con il valore di default definito in *'setup finanza'*. 

A questo punto per creare le fatture cumulative è necessario solo far partire il report, ovvero basterà cercare dal menù *'Fatt. cumulative (PLT)'* e scegliere i filtri che si desiderano.
<img class= "zoom" src = "../img/al-combship/ReportFattCum.png">

La procedura andrà a spaccare in automatico le fatture per i campi di spaccatura obbligatori e per i campi facoltativi del raggruppamento, se presente, della spedizione stessa.  

<!-- Script per usare medium zoom per immagini -->
<script src="../JS/medium-zoom-min.js"></script>
<script src="../JS/medium-zoom-luca.js"></script>