---
title: Dimensioni Estese
layout: application
---
<html>
    <script>
      <!-- DA INSERIRE I RELATIVI LINK -->
        window.onload = function (){
            var instructions = document.getElementById("instructions")
            var termsconditions = document.getElementById("terms&conditions")
            instructions.href="extended-dimensions.html"
            termsconditions.href="extended-dimensions.terms&conditions.html"
        }
    </script>
    <body>
      <div class="card position-fixed d-none d-xl-block" style="width: 18rem; right: 5%;">
        <div class="card-body">
          <!-- TOC -->
          <a href="#dimensioni-estese" class="font-weight-bold">Dimensioni Estese</a><br>
          <a href="#setup">Setup</a><br>
          <a href="#modello-articolo">Modello articolo</a><br>
          <a href="#item-template">Item template</a><br>
          <a href="#traduzioni">Traduzioni</a><br>
          <!-- /TOC -->
        </div>
      </div>
    </body>
</html>


# AL Dimensioni Estese

L'app aggiunge al normale set di 8 dimensioni globali
<!--more--> anche altre 10 dimensioni a livello dell'articolo, del cliente e del fornitore.
Queste serviranno a:
- Configurare articoli in modo più veloce, preciso e dinamico;
- Una ricerca degli articoli più fluida;
- Aggiornare le descrizioni associate ad una stessa dimensione in modo automatico.

## Setup

Affinchè si possa usufruire di queste dimensioni è necessario innanzitutto rendere attiva l'applicazione andando nella
pagina "Dimension Setup" e portando a true il flag "Configure Templ. Item No.".
Nella pagina sono presenti anche i campi: "Def. Description Language Code" per impostare il linguaggio predefinito,
"Def. Country Dimension Code" per le dimensioni di default alla generazione di un nuovo cliente, "Def. Item Dimension
Code" se è compilato, va ad inserire nella dimensione "Item" un nuovo valore dimensione con codice, del valore
dimensione, il codice dell'articolo e per descrizione, la descrizione dell'articolo. Con "Def. Salesperson Dimension
Code" allo stesso modo si andrà a creare un valore nella dimensione "AGENTE" (se impostato) con il codice dell'articolo
e la descrizione.

Dopodiché si potrà proseguire definendo le categorie delle dimensioni nella pagina Dimension Category Setups.

Le categorie possono essere:
- Production Dimension
- Customer Dimension
- Vendor Dimension
  
<img class= "zoom" src = "../img/extended-dimensions-images/2022-07-27.PNG">
<!-- ![](../img/extended-dimensions-images/2022-07-27.PNG)   -->


Da questa pagina è possibile andare a definire le categorie degli articoli, dei clienti e dei fornitori.


## Modello articolo
Le combinazioni di modelli possibili sono innumerevoli, sta all'utente creare i modelli con le caratteristiche di
default che preferisce cosicché il nuovo articolo (o cliente, o fornitore) erediti dei valori precisi.

Esempio:
Nel modello dell'immagine successiva sono stati inseriti alcuni valori di default in alcune delle dimensioni definite
precedentemente nel Dimension Category Setups, per un modello articolo. In questo modo, quando si andrà a generare
l'articolo, verranno impostati per default gli stessi valori presenti nel modello, nelle dimensioni corrispondenti.

<img class= "zoom" src = "../img/extended-dimensions-images/2022-07-26-5.PNG">
<!-- ![](../img/extended-dimensions-images/2022-07-26-5.PNG) -->

Successivamente, impostando "Configuration Mask" sarà possibile generare automaticamente (premendo il bottone "Create
New Item No.") il nome dell'articolo, la descrizione di ricerca e la descrizione dell'articolo.

Questi codici sono generati sulla base di una matrice data dai valori delle dimensioni e modificabili nella scheda
modello. La descrizione dell'articolo è modificabile anche nell'anagrafica articolo, nella parte "Configuration mask".
Se si vuole cambiare il codice, o se lo si vuole abbreviare, è possibile inserire, utilizzando l'azione "Add Suffix to
New Item No.", un progressivo nel codice articolo. In questo modo sarà preservata l'univocità del codice.
L'azione "Item filter with default dimension" dà accesso alla scheda articoli filtrata sui valori impostati nelle
dimensioni.


Nel modo illustrato dall'immagine, nel campo "No. Configuration" per generare il codice dell'articolo, l'applicazione
andrà a sommare tutti i valori presenti nelle prime 4 dimensioni e la settima. E così anche per la descrizione di
ricerca e la descrizione dell'articolo. L'app prende in automatico la descrizione completa (se presente) per la
descrizione dell'articolo e la descrizione abbreviata per la descrizione di ricerca.  

<img class= "zoom" src = "../img/extended-dimensions-images/2022-07-18-4.jpg">
<!-- ![](../img/extended-dimensions-images/2022-07-18-4.jpg)   -->


## Item Template

Nel momento in cui si aggiungerà un nuovo articolo, verrà richiesto all'utente di selezionare un tipo di modello.
Dopo aver scelto il modello desiderato, sarà possibile aggiungere i valori delle dimensioni mancanti per definire al
meglio l'articolo, generare il codice dell'articolo (come spiegato nel paragrafo precedente) e poi aggiungerlo premendo
"OK" in fondo alla scheda.

<img class= "zoom" src = "../img/extended-dimensions-images/2022-07-18-5.jpg">
<!-- ![](../img/extended-dimensions-images/2022-07-18-5.jpg) -->

A questo punto l'articolo è stato generato con tutti i valori delle dimensioni previsti e dallo stesso modello sarà
possibile generare altri articoli.

## Traduzioni

Attraverso quest'applicazione è possibile anche modificare le descrizioni degli articoli (uno o tutte insieme)
attraverso la funzione "Update Item Description". Questa è visibile sia a menù, sia all'interno dell'articolo sotto
"Processo".

<img class= "zoom" src = "../img/extended-dimensions-images/2022-07-20-1.jpg">
<!-- ![](../img/extended-dimensions-images/2022-07-20-1.jpg) -->

<!-- <div style="text-align: center;">

![](./img/extended-dimensions-images/LogoNomeCompl-150x150-180x150.png)
</div> -->
<!-- 
Translation, puoi aggionrarle con la procedura update item description o na cosa simile (sia ceercando dal menù che dall'anagrafica articolo). Le traduzionoi vanno a modificare tutti i nomi delle dimensioni nei vari articoli e le descrizioni ad essi associati costruiti sulla base dei nomi delle dimensioni.


<!-- da sistemare il filtro per dimensioni. -->



<!-- Script per usare medium zoom per immagini -->
<script src="../JS/medium-zoom-min.js"></script>
<script src="../JS/medium-zoom-luca.js"></script>