---
layout: home
---

<html>
<div class="card position-fixed mt-2 d-none d-xl-block" style="width: 18rem; right: 5%;">
  <div class="card-body">
    <!-- TOC -->
    <a href="#privacy-policy" class="font-weight-bold">Privacy Policy</a><br>
    <a href="#1-personal-data-collected-by-the-application">1. Personal Data collected by the Application</a><br>
    <a href="#11-voluntary-contents-and-information-provided-by-the-user">1.1. Voluntary contents and information provided by the User</a><br>
    <a href="#12-data-and-contents-automatically-acquired-while-using-the-application">1.2. Data and contents automatically acquired while using the Application</a><br>
    <a href="#13-personal-data-collected-through-cookies-or-similar-technologies">1.3. Personal Data collected through cookies or similar technologies</a><br>
    <a href="#2-purposes-of-the-processing">2. Purposes of the processing</a><br>
    <a href="#3-personal-data-processing-methods">3. Personal Data processing methods</a><br>
    <a href="#4-legal-basis-for-the-processing">4. Legal basis for the processing</a><br>
    <a href="#5-place">5. Place</a><br>
    <a href="#6-security-of-processing">6. Security of processing</a><br>
    <a href="#7-period-of-storage-of-data">7. Period of storage of Data</a><br>
    <a href="#8-automated-individual-decision-making">8. Automated individual decision-making</a><br>
    <a href="#9-rights-of-the-user">9. Rights of the User</a><br>
    <a href="#10-data-controller">10. Data Controller</a><br>
    <!-- /TOC -->
  </div>
</div>
</html>

Privacy Policy
=
The purpose of this document (hereinafter “Privacy Policy”) is to inform Users with regard to personal data, considered as any information that allows the identification of a natural person (hereinafter “Personal Data”), collected from the website and from the application www.dynamicsapplab.com (hereinafter, the Application).

The Data Controller, as identified below, may amend or simply update, wholly or in part, this Privacy Policy informing the Users about the changes. The amendments or updates shall be binding as soon as they are published on the Application. Therefore, the User is invited to read the Privacy Policy at every access to the Application.

If the User does not accept any amendments to the Privacy Policy, the User must terminate the use of the Application and he may ask the Data Controller to remove his Personal Data.

# 1. Personal Data collected by the Application

The Data Controller shall collect the following categories of Personal Data:

## 1.1. Voluntary contents and information provided by the User

- Contact information and contents: Personal Data that the User voluntarily provides to the Application during use, such as personal details, contact information, login credentials to services and / or products provided, interests and preferences, personal interests and preferences and any other personal contents, etc.

- Sensitive data: Personal Data revealing racial or ethnic origin, political opinions, religious or philosophical beliefs, or trade union membership, and the processing of genetic data, biometric data for the purpose of uniquely identifying a natural person, data concerning health or data concerning concerning the sex life or sexual orientation of a natural person.

- Personal Data from social media: Users may share with the Application the data they have provided to social media. The User can monitor Personal Data, which the Application may have access to through the privacy settings of the relevant social media website. By way of connecting accounts managed by social media to the Application and by way of authorising the Data Controller to have his Personal Data access, the User gives his consent to the acquisition, processing and retention in accordance with the Privacy Policy.

If the User does not communicate Personal Data, for which there is a legal or contractual obligation, will be impossibile to the Data Controller to provide, in whole or in part, its services. It will be impossible also in case that Personal Data is necessary requirement for the use of the service or for the contract conclusion.

The User who communicates to the Data Controller third parties Personal Data, is directly and exclusively liable for their origin, collection, processing, communication or disclosure.

## 1.2. Data and contents automatically acquired while using the Application

- Technical Data: the computer system and the software procedures functional to this Application may acquire, in the course of their ordinary activity, any Personal Data whose communication is implicit in the use of internet communication protocols. Such information is not collected to be associated with identified Users, however, those Data, due to its nature, may identify Users in the Processing and through the association with Data held by third parties. This category includes IP addresses or domain names used by Users who connect to the Application, addresses of Uniform Resource Identifier (URI) of the requested resources, time of the request, method used submitting the request to the server, size of the file obtained, etc.

- Usage Data: Personal Data may be collected relating to the use of the Application by the User, such as the pages visited, the actions performed, the features and services used by the User.

- Geolocation Data: the Application may collect Personal Data concerning User’s location, which may consist in GNSS Data (Global Navigation Satellite System, such as GPS), as well as in data which identify the nearest repeater, Wi-Fi hotspots and bluetooth, communicated when you enable products or features based on the location.

## 1.3. Personal Data collected through cookies or similar technologies

Cookies, or any type of persistent cookies and systems for tracking Users, are not used for the communication of personal information. Therefore, Users’ Personal Data is not collected through the use of such technologies. The use of session cookies (which are not permanently stored on the computer of the User and disappear with the termination of the browser) is strictly limited to the transmission of session identification Data (composed by random numbers generated by the server) necessary to allow a safe and efficient exploration of the Application. Session cookies used in this Application prevent to use other technologies, which may compromise the privacy of Users while browsing on internet and do not allow the acquisition of Personal Data identifying the User.

# 2. Purposes of the processing

 Collected Personal Data may be used for the performance of contractual and pre-contractual obligations and legal obligations as well as for the following purposes:

1. Registration and authentication of the User: to allow the User to register in the Application for access and identification.

2. Support and contact with the User: to answer User’s requests and provide help in case of issues.

3. Comment and feedback: to allow the User to post reviews and comments.

4. Interaction through live chat: to allow the User to interact via live chat.

5. Login through external platforms' accounts: to allow the User to log into the Application using external platforms' accounts (e.g. Google, Facebook).

6. EXTERNAL management of payments via credit card, bank transfer or other means: to manage Users' payments through external platforms that collect payment data without allowing the owner of the Application to access it.

7. Technical monitoring of the infrastructure for maintenance, fixing and improvement of performance: to identify and solve technical issues and improve performance.

8. Statistic only with anonymous Data: to analyse statistics based on aggregated data or data that does not allow to identify the User.

9. Monitoring, analysis and tracking of the User behavior: to monitor and analyse how the User behaves in the Application.

10. Users' profiling: to group and analyse in an automated way the characteristics and the behaviours of the User and provide personalised services and messages.

11. User's experience personalisation: to modify the Application and adapt it to the User's needs.

12. Sending of e-mails or newsletter and handling of mailing list: to contact the User using e-mails containing commercial and promotional information concerning the Application.

# 3. Personal Data processing methods

The Processing of Personal Data is performed with paper, IT and/or digital tools, with methods of organizations and with logics strictly related to the indicated purposes.

In certain cases, subjects other than the Data Controller who are involved in the organization of the Data Controller (such as personnel management, sales personnel, system administrators employees, etc.) or who are not (as IT companies, service providers, postal couriers, hosting providers, etc.) may access to Personal Data. These subjects, will be appointed, where necessary, as Data Processors by the Data Controller and will have access to Users Personal Data whenever required, and shall be contractually obliged to keep it confidential.

The updated list of Data Processors may be requested via email at the email address support@dynamicsapplab.com.

# 4. Legal basis for the processing

User’s Personal Data is processed on the following legal basis:

1. User's consent for one or more specific purposes

2. Processing is necessary for the performance of a contract with the User and/or for the performance of pre-contractual measures

3. Processing is necessary to comply with a legal obligation to which the Data Controller is subject

4. Processing is necessary for the performance of a task carried out in the public interest or for the exercise of public authority vested in the Data Controller

5. Processing is necessary for the purposes of the legitimate interest pursued by the Data Controller or by a third party

6. Processing is necessary for the purposes of the vital interest pursued by the Data Controller or by a third party

It is always possible to ask the Data Controller to clarify the legal basis of each processing at the following mailing address support@dynamicsapplab.com.

# 5. Place

Personal Data are processed in the operative offices of the Data Controller and in any other place in which the parties involved in the Data processing are located. For further information, you may contact the Data Controller at the following email address support@dynamicsapplab.com or at the following mailing address Via Guglielmo Marconi 42, Thiene 36016 (VI) Italia .

# 6. Security of processing

The Data Processing is performed through adequate methods and tools to ensure the Personal Data security and confidentiality, as the Data Controller has implemented appropriate technical and organizational measures which guarantee, in a provable way, that the Processing complies with the applicable law.

# 7. Period of storage of Data

Personal Data will be stored for the time necessary to perform the purposes for which they have been collected.

In particular, Personal Data will be stored for the whole duration of the agreement, to fulfil the inherent and consequent obligations, for the compliance with law provisions and for defensive purposes.

When the Processing of Personal Data is based on the User consent, the Data Controller may store Personal Data until the withdrawal of such consent.

Personal Data may be stored for a longer period of time in order to perform a legal obligation or public Authority order.

All Personal Data shall be deleted or stored in a form that shall not allow the identification of the User within 30 days from the end of the storage period. At the expiration of such period, the right to access, rectify, erase and of portability of Personal Data can not be exercised.

# 8. Automated individual decision-making

All collected Data shall not be subject to automated individual decision-making, including profiling, which may produce legal effects concerning the User or may significantly affect the User.                                

# 9. Rights of the User

The Users may exercise specific rights with respect to Personal Data processed by the Data Controller. In particular, the User has the right to:

1. Withdraw its consent at any time;

2. Object the Processing of its Data;

3. Access its Data;

4. Monitor and request the rectification of Data;

5. Obtain a restriction of Processing;

6. Obtain the erasure or remotion of its Personal Data;

7. Receive its Data or obtain the transfer to a different Data Controller;

8. Lodge a complaint before the supervisory authority for the protection of personal data or start legal proceedings.

In order to exercise their rights, the Users may send a request to the contact information of the Data Controller indicated in this document. These requests are free of charge and performed by the Data Controller in the shortest possible time, in any case no later than 30 days.

# 10. Data Controller

- The Data Controller is Dynamics App Lab srl 

- Registered office in 13 Adelaide Road, Dublin, D02 P950, Ireland

- Company ID 75021 

- E-mail paolo.nicoli@dynamicsapplab.com 

- Phone 0445 163 1277

_Latest update: 06/11/2023_
