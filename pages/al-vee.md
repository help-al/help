---
title: AL VEE
layout: application
---
<html>
    <script>
      <!-- DA INSERIRE I RELATIVI LINK -->
        window.onload = function (){
            var instructions = document.getElementById("instructions")
            var termsconditions = document.getElementById("terms&conditions")
            instructions.href="al-vee.html"
            termsconditions.href="al-vee.terms&conditions.html"
        }
    </script>
    <body>
      <div class="card position-fixed d-none d-xl-block" style="width: 18rem; right: 5%;">
        <div class="card-body">
          <!-- TOC -->
          <a href="#al-vee" class="font-weight-bold">AL VEE</a><br>
          <a href="#11-esigenze">- Esigenze</a><br>
          <a href="#12-funzionalità-principali">- Funzionalità principali</a><br>
          <a href="#13-vantaggi">- Vantaggi</a><br>
          <!-- /TOC -->
        </div>
      </div>
    </body>
</html>


# AL VAT Extended Exemption  

## 1.1. Esigenze

L'esigenza che il modulo si propone di soddisfare è quella di garantire un controllo dettagliato del plafond iva, **per ogni singola dichiarazione di intento**, che un'azienda ha nei confronti di un cliente o che un fornitore ha nei confronti dell'azienda.


## 1.2. Funzionalità principali

Il modulo estende le funzionalità standard di business central che riguardano la gestione delle dichiarazioni d'intento e garantisce l'aggiornamento delle funzionalità fornite alle versioni successive di Business Central (anche in cloud) senza che sia necessario un specifico progetto di migrazione.

Le principali funzionalità offerte dall'app sono le seguenti:

   1. a livello di rilascio di un ordine di vendita (acquisto) il sistema verifica che **la somma degli importi in esenzione** di tutti gli ordini rilasciati per il cliente (fornitore), di tutte le spedizioni (carichi) e di tutte le fatture (note di credito) non superi il plafond previsto per il cliente (fornitore) e nel caso **emette un messaggio che avverte l'utente**.
   2. a livello di spedizione (carico),il sistema verifica che **la somma degli importi in esenzione** di tutte le spedizioni (carichi) e di tutte le fatture (note di credito) non superi il plafond previsto per il cliente (fornitore) e nel caso **emette un messaggio che avverte l'utente**.
   3. a livello di emessione di fattura (Nota di Credito),il sistema verifica che **la somma degli importi in esenzione** di tutte le fatture (note di credito) emesse più **l'importo in esenzione della fattura da registrare** non superi il plafond previsto per il cliente (fornitore) e nel caso **generi un errore** che impedisca alla registrazione di splafonare.
   4. Dalla finestra standard delle esenzioni Iva è possibile, per ogni riga di esenzione collegata al cliente/fornitore visualizzare il dettaglio degli importi dichiarazione d'intento relativi agli ordini, alle spedizioni (carichi) e alle fatture registrate (note di credito), e attraverso il **drill down** degli importi visualizzare la lista dei documenti che concorrono a determinale **l'importo in esenzione**.
   5. Integrazione automatica con l'applicazione per la fatturazione elettronica (ALPA) nel caso si decida di acquistarla e installarla.

## 1.3. Vantaggi

Il controllo dettagliato a livello di singola dichiarazione d'intento consente di impedire che si verifichi un accidentale splafonamento dell'iva in esenzione.  





<!-- Script per usare medium zoom per immagini -->
<script src="../JS/medium-zoom-min.js"></script>
<script src="../JS/medium-zoom-luca.js"></script>
