---
title: AL Finance
layout: application
---
<html>
    <script>
      <!-- DA INSERIRE I RELATIVI LINK -->
        window.onload = function (){
            var instructions = document.getElementById("instructions")
            var termsconditions = document.getElementById("terms&conditions")
            instructions.href="al-finance.html"
            termsconditions.href="al-finance.terms&conditions.html"
        }
    </script>
<body>
    <div class="card position-fixed d-none d-xl-block" style="width: 18rem; right: 5%;">
  <div class="card-body">

<!-- TOC -->
<a href="#al-finance" class="font-weight-bold">AL Finance</a><br>
<a href="#11-introduzione">- Introduzione</a><br>
<a href="#11-fatture-cumulative">- Fatture Cumulative</a><br>


<!-- /TOC -->
  </div>
</div>
</body>
</html>


# AL Finance

## 1.1 Introduzione

Quest'applicazione si prefigge di gestire e controllare al meglio le partite finanziarie (clienti e fornitori). Attraverso pagine specifiche è infatti possibile tenere sotto controllo ogni movimento e i documento correlati.  
Altri vantaggi offerti dall'applicazione sono i seguenti:
- offre la possibilità di utilizzare raggruppamenti spedizioni per generare delle fatture cumulative;
- permette di modificare le scadenze delle fatture già registrate.

## Partite Clienti

Nell'anagrafica del cliente, tra le funzioni *'Naviga'*, è presente il pulsante **Partite Cliente** il quale darà accesso alla pagina di riepilogo delle partite aperte del cliente stesso.

<img class= "zoom" src = "../img/al-fin/PartCli.png">

## Aggiorna scadenze fattura

È possibile cambiare le condizioni di pagamento anche dopo aver registrato la fattura. Per far questo basta entrare nel documento registrato e nella sezione *'Processo'* selezionare **'Aggiorna condizioni pagamento**.

<img class= "zoom" src = "../img/al-fin/AggCondPag.PNG">

Una volta selezionata l'azione, si aprirà la pagina del piano di pagamento. Di default il piano farà riferimento al nr. del documento dal quale si è partiti e con le condizioni di pagamento presenti nel documento.
La schermata che verrà aperta è la seguente.  

<img class= "zoom" src = "../img/al-fin/PianoPag.PNG">

A questo punto è possibile modificare il **Cod. condizione pagam.** scegliendolo dal lookup del campo omonimo. In seguito alla modifica del codice, compariranno le righe dei nuovi movimenti collegate al codice impostato.
Queste righe, modificabili manualmente, suddividono l'importo per le condizioni impostate. È importante che l'*importo del piano* e l'*importo* **coincidano** al centesimo. Quindi se le righe generate hanno tutte l'importo *'1.449,67'*, è necessario togliere un centesimo da una riga affinchè i due importi coincidano.

<img class= "zoom" src = "../img/al-fin/NuovoCodPag.PNG">

Quando si è sicuri delle nuove righe è necessario cliccare su **Registra** per registrare i nuovi movimenti. Questa azione andrà a registrare i nuovi movimenti e a stornare i pagamenti della fattura registrati precedentemente.

<img class= "zoom" src = "../img/al-fin/Reg.PNG">

<font size =1>
pagina in allestimento

<!-- Script per usare medium zoom per immagini -->
<script src="../JS/medium-zoom-min.js"></script>
<script src="../JS/medium-zoom-luca.js"></script>