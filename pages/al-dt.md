---
title: AL DOCUMENT TEMPLATE
layout: application
---
<html>
    <script>
      <!-- DA INSERIRE I RELATIVI LINK -->
        window.onload = function (){
            var instructions = document.getElementById("instructions")
            var termsconditions = document.getElementById("terms&conditions")
            instructions.href="al-dt.html"
            termsconditions.href="al-dt.terms&conditions.html"
        }
    </script>
    <body>
      <div class="card position-fixed d-none d-xl-block" style="width: 18rem; right: 5%;">
        <div class="card-body">
          <!-- TOC -->
          <a href="#al-document-template" class="font-weight-bold">AL DOC-TEMP</a><br>
          <a href="#funzionalità-principali">- Funzionalità principali</a><br>
            <a href="#setup">1 Setup</a><br>
            <a href="#modello-documento">2 Modello documento</a><br>
          <!-- /TOC -->
        </div>
      </div>
    </body>
</html>


# AL Document Template  

## Funzionalità principali

Questa applicazione permette di gestire in modo più sicuro e veloce i documenti di vendita e di acquisto.
Attraverso dei modelli specifici infatti, è possibile impostare dei valori predefiniti per generare dei documenti con i valori scelti.
Questo rende la compilazione del documento sicuramente più veloce, in quanto molti campi sono definibili a setup e permette anche di ovviare a possibili disattenzioni da parte dell'utente.
Inoltre, è possibile impostare una numerazione specifica per ogni modello di documento così da poter distinguere i documenti anche attraverso il numero stesso.

## Setup

Dopo aver installato l'app, per utilizzare le funzionalità proposte dall'applicazione è necessario abilitarle nella pagina *'Setup modello documento'*.
Una volta aperta la pagina e abilitati i flag dei modelli dei documenti di acquisto e di vendita compariranno nella stessa pagina le voci specifiche dei documenti possibili nei quali abilitare o disabilitare le funzionalità.

<img class= "zoom" src = "../img/al-dt/setmoddoc.jpg">

In questo esempio, tutti i modelli sono abilitati perciò, ongi volta che verrà aperto un nuovo documento o anche solo per entrare nella lista dei documenti (per es. gli ordini di vendita, o le fatture di acquisto), verrà chiesto di scegliere uno tra i modelli proposti. La pagina che verrà aperta in seguito è la pagina cercata, filtrata per il tipo di documento scelto.  
Di seguito verrà illustrato nel dettaglio un esempio.

## Modello documento

Tra le funzionalità dei modelli documenti di vendita e di acquisto non c'è differenza, perciò in questa sezione, verrà utilizzato un *ordine di vendita* come esempio.

Come anticipato nel capitolo 'setup', nell'aprire la lista degli ordini di vendita compare una finestra per la scelta tra i modelli presenti per i documenti di vendita.

<img class= "zoom" src = "../img/al-dt/sceltamodedoc.jpg">

Scegliendo un modello, la lista degli ordini di vendita che verrà aperta sarà già filtrata per quel tipo di modello.  
Se si vuole aprire la lista senza filtri basta selezionare *"Annulla"*, verrà poi richiesto all'utente di confermare se si desidera aprire la lista senza il filtro del modello e premendo *"Sì"* verrà aperta.

I modelli di documenti di vendita possono essere creati sia premendo *"Nuovo"* nella pagina di scelta, sia cercando dal menù la pagina *"Modelli documenti vendita"*.

<img class= "zoom" src = "../img/al-dt/moddocvend.jpg">

In questa pagina è possibile creare nuovi modelli di documenti, modificare i modelli presenti, oppure, attraverso il pulsante di scelta rapida **Documenti**, è possibile raggiungere la pagina dei documenti che si desiderano.  
Quando si decide di creare un nuovo modello di documento vendita, la pagina che si aprirà è la seguente:

<img class= "zoom" src = "../img/al-dt/schedamoddocvend.jpg">

In questa pagina è possibile definire i valori che si vogliono mantenere nel modello del documento. Per esempio, se si vuole creare un modello che per default non fa movimenti contabili ma solo di magazzino, basta selezionare **"No Mov. contabili C/G"** nel campo *'Tipo Reg. movimenti'*.

<img class= "zoom" src = "../img/al-dt/schedamoddocvendGEN.jpg">

È possibile anche andare a modificare le impostazioni presenti nel setup contabilità clienti (o fornitori), modifiche che manterrà attive solo per il modello di documento in questione.
Inoltre, nella sezione *'Numerazione'* è possibile definire numerazioni specifiche per ogni tipologia di modello.  
Per esempio nell'immagine successiva, nel modello "NOMOVCONT", per gli ordini di vendita è stata impostata una numerazione specifica 'V-NOMOV'.

<img class= "zoom" src = "../img/al-dt/schedamoddocvendNUM.jpg">
<img class= "zoom" src = "../img/al-dt/ElencoNUM.jpg">

In questo modo, quando si crea un nuovo ordine di vendita scegliendo **'NOMOVCONT'** come modello, verrà proposta automaticamente la numerazione *'V-NOMOV'*; gli ordini di vendita saranno così filtrabili anche per il nr. del documento.

Quando viene creato un nuovo ordine di vendita, se la lista degli ordini è filtrata per un tipo modello, il nuovo oggetto prende automaticamente il tipo modello presente nel filtro e i valori dei campi associati.

<img class= "zoom" src = "../img/al-dt/OrdVend.jpg">


  <font size= 1>
  pagina in allestimento

<!-- Script per usare medium zoom per immagini -->
<script src="../JS/medium-zoom-min.js"></script>
<script src="../JS/medium-zoom-luca.js"></script>
