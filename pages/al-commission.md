---
title: AL Commission
layout: application
---
<html>
    <script>
      <!-- DA INSERIRE I RELATIVI LINK -->
        window.onload = function (){
            var instructions = document.getElementById("instructions")
            var termsconditions = document.getElementById("terms&conditions")
            instructions.href="al-commission.html"
            termsconditions.href="al-commission.terms&conditions.html"
        }
    </script>
    <body>
      <div class="card position-fixed d-none d-xl-block" style="width: 18rem; right: 5%;">
        <div class="card-body">
          <!-- TOC -->
              <a href="#al-commission" class="font-weight-bold">AL Commission</a><br>
          <a href="#a-cosa-serve">- A cosa serve?</a><br>
          <a href="#gerarchie" class="font-weight-bold">- Gerarchie</a><br>
          <a href="#gerarchie-prodotti-clienti-e-agenti">- Gerarchie prodotti, clienti e agenti</a><br>
          <a href="#introduzione" class="font-weight-bold">- Introduzione setup</a><br>
          <a href="#setup-provvigioni-cliente-e-prodotto">- Setup provvigioni clienti e prodotti</a><br>
          <a href="#setup-provvigioni-agente">- Setup provvigioni agente</a><br>         
          <a href="#giornale-registrazioni-provvigioni">- Giornale registrazioni provvigioni</a><br>
             <!-- /TOC -->
        </div>
      </div>
    </body>
</html>

# AL Commission  

## A cosa serve?

Questa applicazione permette di gestire le provvigioni. 
Le caratteristiche principali dell'app sono le seguenti:
 - L'app si fonda sulla possibilità di strutturare le gerarchie dei clienti, prodotti e agenti nel modo più flessibile. E' possibile definire un numero illimitato di gerarchie, ognuna identificata da un nome batch e dalla tipologia della gerarchia (clienti, articoli e agenti) e un numero illimitato di livelli nella gerarchia, fino al dettaglio dell'agente, cliente o articolo.
 - Un setup facile e flessibile che si adatta alle necessità di calcolo dell'azienda. L'impostazione dei setup procede per eccezione da un livello più generale fino al dettaglio della combinazione fra l'agente, una riga della gerarchia cliente e una riga della gerarchia prodotto.
 - I setup delle gerarchie per gli articoli, i clienti e gli agenti sono definibili per periodo di validità, in modo che siano facilmente modificabili le condizioni per il calcolo delle provvigioni per un certo agente senza che questo vada a inficiare il calcolo dei periodi precedenti.
 - Per il calcolo delle provvioni il sistema si basa sui batch delle provvigioni, ognuno dei quali contiene tutti i parametri necessari per il calcolo (agente, tipo di calcolo, tipo periodo, periodo etc.).
 - I batch possono essere generati automaticamente e anche il calcolo delle provvigioni può essere automatizzato.
 - Il calcolo delle provvigioni suggerisce le provvigioni per tutte le righe dei documenti (fatture o note di credito), comprese quelle per cui non c'è un setup attivo. In questo modo l'utente può verificare il setup, eventualmente correggerlo e rilanciare il calcolo con il nuovo setup.
 - Il sistema consente di calcolare le provvigioni per l'agente e la gerarchia dei superagenti collegata all'agente. Le provvigioni di ogni superagente può essere calcolata con una specifica tipologia di provvigione.
  - Il sistema consente di generare automaticamente le fatture di acquisto degli agenti, addebitando il costo della provvigione da liquidare in modo puntuale su ogni riga di spedizione (o reso). 
 - Nella fattura generata le righe sono divise per anno. Oltre alle righe di provvigione come agente, vengono inserite anche le righe corrispondenti alle provvigioni come superagente distinte per ogni subagente.
 - Alla registrazione della fattura di acquisto dell'agente viene registrato anche il corrispondente batch delle provvigioni, dal quale la fattura di acquisto è stata generata. 
 - L'integrazione con l'app AL Enasarco e AL Bonus (contratti per i premi di fine anno), consente di stampare i margini reali al netto degli sconti, dei costi di trasporto, dei premi di fine anno e delle provvigioni.
 - Possibilità di stampare il riepilogo provvigioni per agente, gruppo, insegna, cliente di fatturazione, punto di consegna e articolo. Nel caso in cui venga selezionato il singolo agente è possibile anche inviare automaticamente tramite email il pdf del riepilogo delle provvigioni all'agente. Possibilità di stampare la fattura di acquisto con gli importi corrispondenti al riepilogo delle provvigioni da liquidare all'agente.
 - Possibilità di creare batch per il calcolo delle provvigioni con diversi metodi di calcolo (incassato, fatturato, completamente incassato), periodo (mese, trimestre, semestre, anno) diversi da quelli impostati per gli agenti. Questo consente ad esempio di calcolare e stampare le provvigioni per il fatturato di un intero anno per tutti gli agenti, oppure le provvigioni sul fatturato del III trimestre di un agente che ha invece impostato un periodo di liquidazione delle provvigioni mensile e il calcolo delle provvigioni sull'incassato.


Funzionalità
-
Per un corretto utilizzo dell'applicazione, una volta installata, è necessario impostare il setup di base. Questo si può fare accedendo alla pagina *"General Commission Setup"*.
## General Commission Setup 

Ai fini del calcolo della provvigione è necessario predisporre la pagina *'Setup generale provvigione'* con dei valori di Default come nell'esempio seguente.

<img class= "zoom" src = "../img/al-commission/gencommset.png"> 



Dove i dettagli dei valori sono i seguenti:

<img class= "zoom" src = "../img/al-commission/gencommsetly.png">

Per quanto riguarda l'articolo di default è un articolo creato appositamente per il calcolo delle provvigioni con costo pari a '0'.

## Salesperson Commission Setup
Per impostare il setup dell'agente è necessario solamente accedere alla Salesperson Commission Setup, nella quale è possibile scegliere l'agente, il tipo agente e le relative provvigioni.  
In questa pagina, come l'esempio che segue, bisogna specificare se la scheda dell'agente è fatturabile o meno e la data d'inizio. Mentre nelle righe di setup si possono inserire i valori specifici di provvigione dell'agente utilizzando o meno i criteri proposti.
<img class= "zoom" src = "../img/al-commission/salecommset.png">

Una volta inserite tutte le righe, nella sezione *"Functions"*, è possibile cambiare stato in "Certificato".
<img class= "zoom" src = "../img/al-commission/certification_spa.png">

È possibile creare la scheda per il superagente come nell'esempio che segue:  
<img class= "zoom" src = "../img/al-commission/helena.png">

## Structure Commission List
<img class= "zoom" src = "../img/al-commission/groupfilter.png">  

Nella scheda dell'agente, nella sezione delle righe di setup, è possibile parametrizzare la percentuale di provvigione attraverso dei filtri sui clienti e sui prodotti.  
Questi filtri è possibile aggiungerli nella pagina *"Structure Commission List"*, nella quale si può impostare il tipo origine come cliente o prodotto, il nome del filtro e la descrizione.  
Attraverso questi filtri, perciò, si potranno avere provvigioni diversificate a seconda delle necessità.

<img class= "zoom" src = "../img/al-commission/structcommlist.png">  


## Salesperson Hierarchy Setup  
Attraverso questa pagina, è possibile creare una gerarchia per ogni agente.  
In questo esempio è stato creato un batch: **BC**.  
Successivamente, è stata creata una sovrastruttura per l'agente BC: in questo modo, quando vengono calcolate le provvigioni per l'agente BC, in automatico, vengono calcolate anche le provvigioni dei suoi superagenti. Questo è possibile specificando il tipo di provvigione nella riga struttura.


<img class= "zoom" src = "../img/al-commission/hierarchyset.png">

Una volta definita la struttura è necessario certificarla.

## Sales Header  
Nei documenti di vendita, a questo punto, si può usare l'agente per il quale si è definito il setup e le provvigioni verranno calcolate in automatico.  
Dal pulsante Order &rarr; Salesperson Commission Type è possibile vedere e/o modificare la lista di agenti per i quali verranno calcolate le provvigioni.

<img class= "zoom" src = "../img/al-commission/salescommtype.png"> 

## Commission Journal-Matured 

<img class= "zoom" src = "../img/al-commission/shortcutmatured.png">  

Attraverso i due bottono evidenziati nell'immagine (presa dalla pagina "Salesperson Commision Setup"), è possibile creare ed aprire il batch per le provvigioni maturate.  
<img class= "zoom" src = "../img/al-commission/suggestmatured.png">   
Pigiando il bottone Suggest Matured Commission verranno riportare le provvigioni calcolate per l'agente e i suoi superagenti. 
Una volta verificata la completezza delle righe, è possibile rilasciarle.  

<img class= "zoom" src = "../img/al-commission/releasematured.png">  

## Commission Journal-Liquidated
Dalla pagina *"Salesperson Commission Setup"* si può accedere attraverso i bottoni **Create Commission Batch** e **Open Commission Batch** alla pagina *"Commission Journal"* nella quale si possono gestire le provvigioni dell'agente scelto.  

<img class= "zoom" src = "../img/al-commission/createbatch.png">  

<img class= "zoom" src = "../img/al-commission/journalshortcut.png">  
Con il bottone "Suggest Matured Commissions" verranno popolate le righe con le provvigioni calcolate nel batch maturate. 

<img class= "zoom" src = "../img/al-commission/suggestsalesperscomm.png">

Da qui è possibile gestire riga per riga gli importi specifici per ogni documento.
















<!-- 
Funzionalità
- 

Nella pagina *"Setup Generale Provvigioni"* è possibile impostare la preferenza della modalità di calcolo delle provvigioni. 
Le modalità possibili sono: 
- Gerarchia;
- Sequenza.  
  
La prima darà all'utente la possibilità gestire i propri gruppi di gerarchie e il compilatore sceglierà su una base di criteri preimpostati la riga di provvigione da calcolare. Mentre il secondo, lavora sulla una logica di priorità, ovvero farà scegliere al compilatore la riga con i filtri corretti e la priorità più alta. 

### Gerarchie

Per impostare i setup delle provvigioni, si possono sfruttare le **gerarchie**, ovvero la possibilità di memorizzare un insieme di informazioni strutturate ad **albero** all'interno di batch liberamente definibili dall'utente. Esistono diverse tipologie di gerarchia:
  - le gerarchie dei prodotti; 
  - le gerarchie dei clienti;
  - le gerarchie degli agenti.

La gerarchia si basa sul concetto di **struttura**, ovvero una lista di informazioni che possono essere collegate a diverse origini: 
  - Clienti (quando si vuole selezionare un cliente specifico);
  - Articoli (quando si vuole selezionare un articolo specifico);
  - Agenti (quando si vuole selezionare un agente specifico);
  - Gruppo (quando non si vuole legare la struttura ad un'origine specifica).

Questa procedura, nel momento in cui è sufficiente un solo superagente, può essere eseguita automaticamente dal compilatore durante l'inserimento del documento di vendita. 
<a href="#setup-gerarchia-superagenti">Per saperne di più.</a>


### Struttura

Nella pagina *Lista strutture* è possibile modificare, inserire e cancellare le strutture di ogni tipo. Queste righe mettono in relazione il tipo della struttura (Clienti, Prodotti, Agenti), il nome della struttura, la descrizione, il tipo di origine e il nr. origine (quando il tipo origine non è "Gruppo").
Di seguito, un esempio della *lista strutture*.  


<img class= "zoom" src = "../img/al-commission/listastr.jpg">


<!-- ![](../img/al-commission/listastr.jpg)
Setup gerarchia prodotti, clienti, agenti
- 
### Gerarchie prodotti, clienti e agenti
Le pagine in cui è possibile impostare le gerarchie per i singoli elementi sono: 
- *Setup gerarchia prodotto* per quanto riguarda i prodotti;
- *Setup gerarchia cliente* per i clienti;
- *Setup gerarchia agente* per gli agenti.  
  
Per creare nuove strutture, da inserire nelle gerarchie, basta fare il lookup dal campo *Nome struttura*, successivamente selezionare *Seleziona da elenco completo*, *Nuovo* e inserire la nuova struttura.  
Una volta creata la **gerarchia** è necessario **certificare** il batch creato. Per far questo basta cliccare su *processo* e poi *certifica*. 

Per esempio, una gerarchia per quanto riguarda i *prodotti finiti* potrebbe essere la seguente.  


<img class= "zoom" src = "../img/al-commission/setgerprod.jpg">



In questo esempio è stato creato un batch, **PRODOTTI**, nel quale possono essere inclusi tutti gli articoli. 
Successivamente, è stata creata una sottostruttura per gli articoli finiti: in questo modo, quando viene inserito un nuovo prodotto, è possibile collegarlo alla struttura figlio che a sua volta è collegata alla struttura padre. Perciò, nel momento in cui si andrà a ricercare un prodotto con Map ID collegato agli articoli finiti, se il sistema non trova una riga provvigione corrispondente a tutti i parametri, automaticamente cercherà una riga che corrisponda alla struttura prodotto subito sopra (in questo caso: *tutti gli articoli*).

Allo stesso modo è possibile fare per i clienti. Ecco un esempio di una gerarchia cliente.

<img class= "zoom" src = "../img/al-commission/setgercli.jpg">


Anche in questo esempio è stato creato un batch per tutti i clienti. Come si può notare dall'immagine, alla stessa struttura padre è possibile associare più di una sottocategoria. È anche possibile associare ad una struttura che è già figlio di una struttura più grande, una struttura più specifica, figlia di quest'ultima.

Per gli **agenti** si può procedere allo stesso modo, con l'unica differenza che per l'agente è possibile impostare il *'tipo di provvigione'*.  
Questo permette di gestire il **calcolo delle provvigioni** facendo riferimento al tipo di provvigione con la quale si vuole procedere al calcolo. Perciò per l'agente *'CB'* dell'immagine seguente, quando verrà calcolata la provvigione per l'agente *'CM'*, verrà calcolata anche la sua provvigione di tipo *'SUPERAGENTE1'*. Mentre se venisse calcolata la provvigione direttamente di *'CB'*, verrebbe calcolata come tipo *'AGENTE'*.  

<img class= "zoom" src = "../img/al-commission/Setgerage.jpg">


 

Il **tipo provvigione** è inseribile, modificabile e cancellabile attraverso la pagina *Elenco dei tipi di provvigione*. 

Sequenza
- 
### Sequenza

Questa modalità permette all'utente una maggior libertà in quanto non è più necessario impostare per ogni cliente e prodotto una gerarchia o associare un Map Id. Questo metodo lavora con i filtri, perciò se un filtro non dovesse essere impostato, il compilatore semplicemente manterrà tra le possibili righe da scegliere tutte le righe, per poi scegliere quella con la priorità più alta.  

La priorità è possibile definirla nella **'Scheda di Setup provvigione Agente'**: all'interno della pagina infatti, è possibile trovare la sezione *'Righe di Setup'* in cui è possibile definire la voce *'Sequenza'*. 

<img class= "zoom" src = "../img/al-commission/RigheSetPRov.jpg">

Questo campo, come suggerisce il nome, scandirà la sequenza e quindi l'ordine di priorità, delle righe di provvigione che si stanno inserendo per quel determinato Agente. In questo modo le provvigioni verranno calcolate rispetto ai filtri immessi nella riga con priorità più alta.

Setup
- 
### Introduzione 

Fin'ora abbiamo parlato solo di ciò che riguarda le gerarchie, ovvero le associazioni tra strutture appartenenti o agli agenti, o ai prodotti, o ai clienti. Ora analizziamo invece ciò che riguarda le associazioni tra le varie gerarchie.
Per calcolare la provvigione, viene utilizzato il setup definito nel *Lista setup provvigioni*. 
Questa pagina è raggiungibile sia cercando la stessa da menù, sia dalla pagina di *setup provvigione agente* nelle righe di setup, in cui i campi *'Cod. agente'*, *'Nr. tipo provvigione'* e *'starting date'* sono già impostati con i valori dell'agente stesso.
Il sistema è in grado di scegliere la riga di dettaglio maggiore, cosicché, ad esempio, se esistessero due righe di setup applicabili per il calcolo delle provvigioni, una definita per l'intera GDO e l'altra per un singolo punto vendita, il sistema utilizzerà la riga corrispondente al punto vendita per calcolare la provvigione. Allo stesso modo, se esistono più righe di setup, una riferita a tutti i prodotti e un'altra a quella dei "private label", il sistema selezionerà la riga di setup corrispondente agli articoli private label.

<img class= "zoom" src = "../img/al-commission/setprov.jpg">


Da questa pagina (*'Setup provvigioni'*) è possibile vedere le associazioni tra agenti, tipo di provvigione, data inizio e data fine e la riga di gerarchia con la quale si vuole calcolare la provvigione sia dei clienti che dei prodotti.

### Setup provvigioni cliente e prodotto

Attraverso le pagine ***Setup provvigioni cliente*** e ***Setup provvigioni prodotto*** è possibile definire un'associazione tra il cliente o il prodotto, una data di inizio e un collegamento ad una gerarchia specifica a cui far riferimento nel momento del calcolo delle provvigioni.  
Questo campo è importante nel momento in cui si vuole comprendere nel calcolo l'intera gerarchia e quindi anche le strutture che nel batch della gerarchia scelta sono nei livelli più alti.
Ad esempio nell'immagine seguente se prendiamo in considerazione le prime due righe possiamo notare che, a due clienti diversi, con le relative date di inizio, sono associate due diverse righe struttura. 

<img class= "zoom" src = "../img/al-commission/setprovcli.jpg">


Riprendendo l'immagine del *setup gerarchia clienti*, possiamo notare che entrambe puntano alla struttura padre *'TUTTI'*.  
Questa associazione implica che nel calcolo delle provvigioni, se non viene trovata la riga in cui è presente il dettaglio maggiore, si procederà per livelli sempre più alti fino a trovarne una di adatta. 

### Setup provvigioni agente

Allo stesso modo, attraverso la pagina *'setup provvigioni agente'*, si può defnire il **setup delle provvigioni** in cui si associa un *agente*, con un *tipo di provvigione* e una *data* di inzio validità.  
Tuttavia, rispetto ai clienti e ai prodotti, la gerarchia dell'agente non è importante solo per i livelli, ma anche per il calcolo delle provvigioni stesse. Infatti, attraverso la gerarchia dell'agente, il sistema andrà automaticamente a calcolare anche le provvigioni dei superagenti associati a quell'agente.

<img class= "zoom" src = "../img/al-commission/schedasetprovage.jpg">


In ognuna delle tre pagine è presente il pulsante "Delete Map ID" in modo che si possa sempre cancellare il riferimento alla gerarchia. Questo pulsante, nella pagina Setup provvigione agente si trova in *'Correlato'*, *'Agente'*.  
Nei *SuperAgenti* non è necessario inserire il MAP ID, in quanto la provvigione generalmente non viene calcolata partendo da un superagente, ma da un agente, perciò l'agente avrà già tutti i riferimenti necessari.

Per ogni periodo, è possibile impostare uno solo dei setup attivi con il flag **fatturabile** (il setup è attivo quando è nel periodo compreso tra la sua data inizio e la sua data fine).   
Questo flag, insieme al campo **nr. fornitore** serve per identificare i batch per il calcolo delle provvigioni collegati al setup, dai quali si possono generare le fatture di acquisto per le provvigioni dell'agente.  

Sulla lista dei setup delle provvigioni agente sono presenti vari pulsanti che consentono di:  
<a href="#filtrare-i-setup-provvigioni-agente-attivi-rispetto-alla-workdate">- Filtrare i setup provvigioni agente attivi rispetto alla workdate</a><br>
<a href="#creare-i-batch-per-il-calcolo-delle-provvigioni">- Creare i batch per il calcolo delle provvigioni</a><br>
<a href="#calcolare-le-provvigioni-per-una-certa-selezione-di-batch">- Calcolare le provvigioni per una certa selezione di batch</a><br>
<a href="#accedere-a-diversi-setup-per-la-gerarchia-dei-clienti-dei-prodotti-e-degli-agenti">- Accedere a diversi setup per la gerarchia dei clienti, dei prodotti e degli agenti</a><br>

Setup Gerarchia Superagenti
- 
### Setup gerarchia superagenti

Per utilizzare questa funzionalità, è sufficiente creare un documento di vendita con l'agente e un superagente nella sezione  **'Provvigione'**. 
Una volta che vengono validati entrambi i campi, il sistema chiederà all'utente la conferma per inserire l'Id map agente collegato sia al codice agente che al codice superagente presenti nel documento. Se il compilatore non trova un batch con quel superagente e quel subagente, lo creerà automaticamente. 

<img class= "zoom" src = "../img/al-commission/OrdvendProv.jpg">




È necessario impostare il tipo di provvigione di default del superagente nella pagina di **'Setup generale provvigione'**. Il nuovo batch avrà come *'Nome batch'* il nome del superagente impostato e come riga struttura l'agente definito nel documento.

Calcolo delle provvigioni
-

Ai fini del calcolo della provvigione è necessario impostare anche la pagina *'Setup generale provvigione'* con dei valori di Default come nell'esempio seguente.

<img class= "zoom" src = "../img/al-commission/setgenprov.jpg">


Dove i dettagli dei valori sono i seguenti:

<img class= "zoom" src = "../img/al-commission/setgenprovDettaglio.jpg">


Per quanto riguarda l'articolo di default è un articolo creato appositamente per il calcolo delle provvigioni con costo pari a '0'.

Nella pagina **Giornale registrazioni provvigioni**, nella sezione *funzioni*, ci sono i pulsanti:
- Suggerisci provvigione agente
- Suggerisci provvigioni maturate
- Rilascia
- Riapri
- Salta il calcolo delle provvigioni
- Crea fattura di acquisto

<img class= "zoom" src = "../img/al-commission/giornregprov.jpg">

### Suggerisci provvigioni maturate
Per utilizzare questa funzionalità è importante che il batch selezionato sia di tipo *'Provvigione maturata'*. 
Una volta creato il batch per le provvigioni maturate (per sapere come creare un batch dal giornale provvigioni, <a href="#crea-batch-dal-giornale">clicca qui</a>), è possibile servirsi della funzione che suggerisce le provvigioni maturate.  Questa funzione va a calcolare tutte le provvigioni a seconda dei criteri impostati nel batch. 
### Rilascia
Con questa funzione è possibile rilasciare il batch delle provvigioni maturate. Una volta rilasciato, tutte le righe presenti nel batch verranno salvate nella pagina Mov. provvigioni maturate.
### Riapri
Nel caso si desiderasse riaprire un batch già rilasciato, verrà chiesto all'utente la conferma in quanto verranno elimate tutte le righe dei movimenti delle provvigioni maturate già salvate. Per risalvarle basterà rilasciare nuovamente il batch.
### Suggerisci provvigione agente
Questo pulsante permette di prelevare le righe, associate ai criteri del batch selezionato, dalla pagina **Mov. provvigioni maturate**. 
### Salta il calcolo delle provvigioni
Dopo essersi posizionati su una riga **specifica** delle provvigioni calcolate, questa funzione ha lo scopo di andare ad impostare, nella riga corrispondente dei documenti di vendita, il valore del campo *'Salta calcolo provvigione'* a *true*. In questo modo, ricalcolando le provvigioni, la riga verrà saltata.
Invece, nel caso contrario, per impostare il flag a false è necessario andare nella pagina dei documenti di vendita e utilizzare la funzione **Ripristina salta calcolo provvigione**.
### Crea fattura di acquisto
Per quanto riguarda **Crea fattura di acquisto**, è una funzione che permette di generare la fattura associata alle righe provvigione dell'agente già presenti nel batch. 

Funzionalità Setup Provvigione Agente
- 
### Filtrare i setup provvigioni agente attivi rispetto alla workdate

Cliccando sul pulsante il sistema filtra automaticamente tutte le righe di setup provvigioni agenti che hanno una data inizio minore uguale alla workdate e una data fine maggiore (o vuota).

### Creare i batch per il calcolo delle provvigioni

Questo pulsante permette di filtrare le righe provvigione per creare un batch contenente le righe associate ai filtri impostati.  
Per esempio si possono filtrare le righe nel seguente modo:  

<img class= "zoom" src = "../img/al-commission/Creabatch.jpg">


In questo modo il batch risultante conterrà tutte e solo le righe provvigioni collegate all'agente *'AM'*, con il tipo di provvigione *'Agente'*, con il periodo di pagamento *'Trimestre'* e il tipo di pagamento *'Incassato'*. È possibile aggiungere anche i parametri di creazione, ovvero paramretri con i quali il sistema andrà a creare il batch. 
Una volta creato il batch è possibile aprirlo con il pulsante *'Apri batch provvigione'*. Nel caso ce ne sia più di uno verrà richiesto all'utente di selezionarne uno, come nell'immagine seguente.  

<img class= "zoom" src = "../img/al-commission/sceltabatchprov.jpg">

### Calcolare le provvigioni per una certa selezione di batch

Per calcolare più batch contemporaneamente basta selezionare, dalla pagina *'Setup provvigioni agente'*, più righe e con il pulsante **Calcolare i giornali di registrazione provvigioni** il sistema andrà a calcolare tutte le righe di provvigione di tutti i batch creati precedentemente per gli agenti selezionati. 
### Accedere a diversi setup per la gerarchia dei clienti, dei prodotti e degli agenti

Dalla pagina *'Setup provvigioni agente'* è possibile accedere in modo rapido anche agli altri setup come per esempio il setup provvigione clienti o prodotti. Come si può vedere dall'immagine infatti, ci sono i collegamenti alle varie pagine nella testata della pagina.

<img class= "zoom" src = "../img/al-commission/setprovage.jpg">


### Crea batch dal giornale

Per creare un nuovo batch dalla pagina **Giornale registrazione provvigioni**, basta cliccare nel lookup del nome del batch. Da qui poi, facendo *Nuovo*, sarà possibile inserire i valori per il batch desiderato come nell'esempio che segue. Il **nome del batch** non è necessario inserirlo, in quanto viene generato automaticamente una volta aggiunti i valori del batch.

<img class= "zoom" src = "../img/al-commission/ProvGiorBatch.png"> -->

<!-- Script per usare medium zoom per immagini -->
<script src="../JS/medium-zoom-min.js"></script>
<script src="../JS/medium-zoom-luca.js"></script>
