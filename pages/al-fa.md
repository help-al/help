---
title: AL Foundation App
layout: application
---
<html>
    <script>
      <!-- DA INSERIRE I RELATIVI LINK -->
        window.onload = function (){
            var instructions = document.getElementById("instructions")
            var termsconditions = document.getElementById("terms&conditions")
            instructions.href="al-fa.html"
            termsconditions.href="al-fa.terms&conditions.html"
        }
    </script>
<body>
    <div class="card position-fixed d-none d-xl-block" style="width: 18rem; right: 5%;">
  <div class="card-body">

<!-- TOC -->
<a href="#al-fa" class="font-weight-bold">AL FA</a><br>
<a href="#11-introduzione">- Introduzione</a><br>


<!-- /TOC -->
  </div>
</div>
</body>
</html>


# AL Foundation App

## 1.1 Introduzione
Quest'applicazione fornisce funzionalità di base per l'azione di altre applicazioni, le quali, integrandosi con sistemi standard, permettono di gestire al meglio la propria società.



<font size =1>
pagina in allestimento

<!-- Script per usare medium zoom per immagini -->
<script src="../JS/medium-zoom-min.js"></script>
<script src="../JS/medium-zoom-luca.js"></script>